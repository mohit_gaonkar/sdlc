package com.mohit.jms.adapter;

import java.rmi.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mohit.jms.listener.ConsumerListener;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerListener.class
			.getName());

	public void sendToMongo(String json) throws UnknownHostException {
		// TODO Auto-generated method stub
		logger.info("sending to MongoDB");

		
		MongoClient client = new MongoClient();
		@SuppressWarnings("deprecation")
		DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("contact");
		logger.info("converting json to db object");
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
		logger.info("sent to mongodb");
	}

}
